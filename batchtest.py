from apacheconfig import *


class BatchJobSpec:
    
    def __init__(self, pythoncode , batch):
        self.pythoncode = pythoncode
        self.name = batch.get("name")
        self._exec = ['foo', 'bar']
        
    @property
    def exec(self):
        locals_ = {'self': self}
        exec(self.pythoncode, locals_)
        for attrib in self._exec:
            print('__setattr__({})'.format(attrib))
            self.__setattr__(attrib, locals_[attrib])

with make_loader() as loader:
    config = loader.load('batch.spec')


batchjobspec = BatchJobSpec(config.get("python"), config.get("batch"))
print(batchjobspec.pythoncode)
print(batchjobspec.name)
print(batchjobspec.exec)
print(batchjobspec.foo())
print(batchjobspec.bar())
